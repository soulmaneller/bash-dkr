#!/bin/bash

if [ ! -f .docker ]; then
    echo "Please enter project name: "
    read project_name
    echo "PROJECT_NAME=${project_name}" > .docker
    echo "DOCKER_COMPOSE_FILE=\"docker-compose.yml\"" >> .docker
fi

. .docker
if [ -z $PROJECT_NAME ]; then
    echo "Variable 'PROJECT_NAME' is invalid"
    exit
fi

if [ -z $DOCKER_COMPOSE_FILE ]; then
    DOCKER_COMPOSE_FILE="docker-compose.yml"
fi

cmd=""
# docker_compose_file=docker/${PROJECT_NAME}.yml
# if [ ! -f "$docker_compose_file" ]; then
#     echo "Project name not exists"
#     exit 1
# fi
# shift
#

run_docker_cmd() {
    command="docker-compose -f ${DOCKER_COMPOSE_FILE} -p '${PROJECT_NAME}' $@"
    echo $command
    eval $command
}

start_docker() {
    export COMPOSE_HTTP_TIMEOUT=28800
    run_docker_cmd up -d $@
    run_docker_cmd logs -f --tail=20 $@
}

if [ $1 == "logs" ]; then
    cmd="logs -f --tail=20"
    shift
fi

if [ "$1" == "boot" ]; then
    # start $@
    shift
    start_docker $@
elif [ "$1" == 'reboot' ]; then
    shift
    run_docker_cmd down --remove-orphans
    start_docker $@
elif [ "$1" == 'down' ]; then
    run_docker_cmd down --remove-orphans
elif [ "$1" == 'recreate' ]; then
    shift
    run_docker_cmd up -d --force-recreate $@
else
    run_docker_cmd $cmd $@
fi
