this script for shorten docker compose command and support for run multiple projects

# Installation

add command to alias `~/.bash_alias` ie. `alias dkr="~/scripts/dkr/dkr.sh`

# Usage

At the first time of running project it'll ask you for entering project name, then it'll create file `.docker` as configuration

## commands

**boot**: Start all services in docker-compose.yml as background (-d) and show log for all containers

**reboot**: Down and Up all services

**logs** *[container1 container2 ...]*: Log all services, support logs specify container

**up**: Same as `docker-compose up` command

**down**: Same as `docker-compose down` command

**restart** *[container1 container2 ...]*: Same as `docker-compose restart` command
